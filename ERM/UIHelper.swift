//
//  UIHelper.swift
//  ERM
//
//  Created by Anders Cheow on 22/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit
import Toaster

class UIHelper {
    static func showAlert(_ vc: UIViewController, message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: R.string.localizable.action_ok(), style: .default, handler: nil))
        vc.present(alertVC, animated: true, completion: nil)
    }
    
    static func showDismissableAlert(_ vc: UIViewController, message: String, completion: (() -> Void)? = nil) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: R.string.localizable.action_ok(), style: .default, handler: { (action) in
            completion?()
            alertVC.dismiss(animated: true, completion: nil)
        }))
        vc.present(alertVC, animated: true, completion: nil)
    }
    
    static func showAlert(_ nav: UINavigationController?, message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: R.string.localizable.action_ok(), style: .default, handler: nil))
        nav?.present(alertVC, animated: true, completion: nil)
    }
    
    static func showDismissableAlert(_ nav: UINavigationController?, message: String, completion: (() -> Void)? = nil) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: R.string.localizable.action_ok(), style: .default, handler: { (action) in
            completion?()
            alertVC.dismiss(animated: true, completion: nil)
        }))
        nav?.present(alertVC, animated: true, completion: nil)
    }
    
    static func toast(_ message: String) {
        Toast.init(text: message, duration: Delay.long).show()
    }
}
