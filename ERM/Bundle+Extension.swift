//
//  Bundle.swift
//  ERM
//
//  Created by Anders Cheow on 06/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

private var kBundleKey: UInt8 = 0

final class BundleEx: Bundle {
    
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        return (objc_getAssociatedObject(self, &kBundleKey) as? Bundle)?.localizedString(forKey: key, value: value, table: tableName) ?? super.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    
    static let once: Void = {
        object_setClass(Bundle.main, type(of: BundleEx()))
    }()
    
    class func setLanguage(lang: String) {
        Bundle.once
        UserDefaults.standard.set([lang], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        let value = Bundle.init(path: (Bundle.main.path(forResource: lang, ofType: "lproj"))!)
        objc_setAssociatedObject(Bundle.main, &kBundleKey, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}
