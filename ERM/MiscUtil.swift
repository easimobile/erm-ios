//
//  MiscUtil.swift
//  ERM
//
//  Created by Anders Cheow on 27/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import LocalAuthentication

let languages = ["zh-Hans" : "中文", "en" : "English"]

class MiscUtil {
    
    static func gotoSettingsPage() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }
    
    static func requestCameraPermission(authorised: @escaping () -> Void, unauthorised: @escaping () -> Void) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status{
        case .authorized:
            Logger.log("requestCameraPermission: authorized")
            authorised()
        case .notDetermined:
            Logger.log("requestCameraPermission: notDetermined")
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    authorised()
                } else {
                    unauthorised()
                }
            }
        case .denied, .restricted:
            Logger.log("requestCameraPermission: denied | restricted")
            unauthorised()
        default:
            break
        }
    }
    
    static func delay(_ deadline: DispatchTime, _ completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: {
            completion()
        })
    }
}
