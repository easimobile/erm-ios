//
//  SplashCoordinator.swift
//  ERM
//
//  Created by Anders Cheow on 14/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

protocol SplashCoordinatorDelegate {
    func gotoLoginScene()
    func gotoMainScene()
}

class SplashCoordinator: NSObject, Coordinator {
    
    fileprivate let presenter: UINavigationController
    
    var delegate: SplashCoordinatorDelegate?
    
    init(_ presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    func start() {
        self.showSplashScene()
        self.checkVersion()
    }
    
    fileprivate func showSplashScene() {
        Logger.log()
        let splashVc = R.storyboard.splashScene.instantiateInitialViewController()!
        splashVc.delegate = self
        self.presenter.pushViewController(splashVc, animated: false)
    }
    
    fileprivate func checkVersion() {
        apiProvider.customRequest(Api.checkVersion(model: CheckVersionRequest()), whenSuccess: { (result: Bool) in
            Logger.log("Version checked: \(result)")
            if result {
                self.showUpdateAppDialog()
            } else {
                self.checkIsAuthenticated()
            }
        }, whenError: { error in
            self.checkIsAuthenticated()
        })
    }
    
    fileprivate func showUpdateAppDialog() {
        let alert = UIAlertController(title: nil, message: R.string.localizable.prompt_update_app(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: R.string.localizable.action_upgrade_now(), style: .default, handler: { (alert) -> Void in
            // open app store
            UIApplication.shared.open(URL(string: "itms-apps://itunes.apple.com/app/id\(Configuration.APP_STORE_ID)")!, options: [:], completionHandler: { (_) in
                exit(0)
            })
        })
        alert.addAction(okAction)
        self.presenter.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func checkIsAuthenticated() {
        Logger.log()
        if Defaults[.app_loggedIn] {
            self.delegate?.gotoMainScene()
        } else {
            self.delegate?.gotoLoginScene()
        }
    }
}

// MARK:- SplashViewControllerDelegate

extension SplashCoordinator: SplashViewControllerDelegate {
    
}
