//
//  BaseRequest.swift
//  ERM
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

open class BaseRequest {
    var apiKey: String?
    
    init(apiKey: String? = Defaults[.app_accessToken]) {
        self.apiKey = apiKey
    }
    
    func toDict() -> [String : Any] {
        var dict = [String : Any]()
        if let token = self.apiKey {
            dict["apiKey"] = token
        }
        return dict
    }
}
