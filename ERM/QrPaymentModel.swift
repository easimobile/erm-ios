//
//  QrPaymentModel.swift
//  DDC
//
//  Created by Anders Cheow on 28/05/2019.
//  Copyright © 2019 J Powersoft Solutions. All rights reserved.
//

import Foundation

struct QrPaymentGenerate: Codable {
    let username: String
    let card_number: String
}

struct QrPaymentScan: Codable {
    let sender_username: String
    let sender_card_number: String
    let recipient_username: String
    let recipient_card_number: String
    let balance: Double
}
