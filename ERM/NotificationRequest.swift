//
//  NotificationRequest.swift
//  ERM
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

public class RegisterPushTokenRequest : BaseRequest {
    var pushToken: String
    
    init(pushToken: String) {
        self.pushToken = pushToken
    }
    
    override func toDict() -> [String : Any] {
        var dict = super.toDict()
        dict["pushToken"] = self.pushToken
        dict["action"] = "create"
        dict["os"] = "ios"
        dict["app"] = "ddcard"
        return dict
    }
}

public class RemovePushTokenRequest : BaseRequest {
    var pushToken: String
    
    init(pushToken: String) {
        self.pushToken = pushToken
    }
    
    override func toDict() -> [String : Any] {
        var dict = super.toDict()
        dict["pushToken"] = self.pushToken
        dict["action"] = "destroy"
        dict["os"] = "ios"
        dict["app"] = "ddcard"
        return dict
    }
}
