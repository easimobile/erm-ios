//
//  AppDelegate.swift
//  ERM
//
//  Created by Anders Cheow on 30/04/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import UserNotifications
import IQKeyboardManagerSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?
    
    private var pendingPushNotification: PushNotification?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.setupConfiguration(application, launchOptions)
        self.startApplication()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Logger.log("error: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(.newData)
    }
    
    fileprivate func startApplication() {
        // start ui
        let window = UIWindow(frame: UIScreen.main.bounds)
        let appCoordinator = AppCoordinator(window: window)
        self.window = window
        self.appCoordinator = appCoordinator
        appCoordinator.start()
    }
    
    fileprivate func setupConfiguration(_ application: UIApplication, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        self.setupStyle()
        self.setupDefaultLanguage()
        self.setupIQKeyboardManager()
        self.setupFirebase()
        self.setupPushNotification(application)
    }
    
    fileprivate func setupStyle() {
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = UIColor(hexString: "#F8595A")
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(hexString: "#A3A2A4")], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: .selected)
    }
    
    fileprivate func setupDefaultLanguage() {
        Bundle.setLanguage(lang: Defaults[.app_languageCode])
    }
    
    fileprivate func setupIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
    }
    
    fileprivate func setupFirebase() {
        FirebaseApp.configure()
    }
    
    fileprivate func setupPushNotification(_ application: UIApplication) {
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Logger.log("received notification in foreground \(notification.request.content.userInfo)")
        // Increment notification count
        NotificationCountService.instance.incrementCount()
        // get data
        let userInfo = notification.request.content.userInfo
        let pushNotification: PushNotification? = self.getPushNotification(with: userInfo)
        
        if let pushNotification = pushNotification, let appCoordinator = self.appCoordinator {
            appCoordinator.updateNotifications(pushNotification)
        }
        // show as system notification
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        Logger.log("user tapped on system notification")
        // Decrement notification count
        NotificationCountService.instance.decrementCount()
        // get data
        let userInfo = response.notification.request.content.userInfo
        let pushNotification: PushNotification? = self.getPushNotification(with: userInfo)
        
        if let pushNotification = pushNotification {
            self.markNotificationAsViewed(pushNotification.notification_id)
            
            if let appCoordinator = self.appCoordinator {
                appCoordinator.routePushNotification(pushNotification)
            } else {
                Logger.log("app coordinator not ready, push notification handling postponed")
                self.pendingPushNotification = pushNotification
            }
        }
        
        completionHandler()
    }
    
    fileprivate func getPushNotification(with userInfo: [AnyHashable : Any]) -> PushNotification? {
        let notificationId: Int = Int(userInfo["notification_id"] as! String)!
        let itemId: String? = userInfo["item_id"] as? String
        var category: String?
        var additionalInfo: AdditionalInfo?
        
        if let addInfo = userInfo["additional_info"] as? String, let addInfoObject = try? JSONDecoder().decode(AdditionalInfo.self, from: Data(addInfo.utf8)) {
            additionalInfo = addInfoObject
        } else {
            additionalInfo = nil
        }
        
        if let cat = userInfo["category"] as? String {
            category = cat
        } else {
            category = nil
        }
        
        if let category = category {
            return PushNotification(notification_id: notificationId, item_id: itemId, category: category, additional_info: additionalInfo)
        }
        
        return nil
    }
    
    fileprivate func markNotificationAsViewed(_ notificationId: Int) {
        
    }
}

extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        Logger.log("fcm token: \(fcmToken)")
        NotificationTokenService.instance.registerOrRefreshPushToken(fcmToken)
    }
    
}
