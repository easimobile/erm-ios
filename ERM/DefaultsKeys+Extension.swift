//
//  DefaultsKeys+Extension.swift
//  ERM
//
//  Created by Anders Cheow on 30/04/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import SwiftyUserDefaults

extension DefaultsKeys {
    static let app_loggedIn = DefaultsKey<Bool>("app_loggedIn", defaultValue: false)
    static let app_languageCode = DefaultsKey<String>("app_languageCode", defaultValue: "en")
    static let app_accessToken = DefaultsKey<String?>("app_accessToken")
    static let app_isPushTokenRegistered = DefaultsKey<Bool>("app_isPushTokenRegistered", defaultValue: false)
    static let app_registeredPushToken = DefaultsKey<String?>("app_registeredPushToken")
    static let app_notificationCount = DefaultsKey<Int>("app_notificationCount", defaultValue: 0)
}
