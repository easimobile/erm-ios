//
//  MainCoordinator.swift
//  ERM
//
//  Created by Anders Cheow on 22/08/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import UIKit

protocol MainCoordinatorDelegate {
    func didLogout()
}

class MainCoordinator: NSObject, Coordinator {
    
    fileprivate let presenter: UINavigationController
    var delegate: MainCoordinatorDelegate?
    
    var tabController: MainTabBarController?
    fileprivate var viewControllers: [UIViewController]?
    
    init(_ presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    func start() {
        Logger.log()
        // init tab controller
        self.tabController = MainTabBarController()
        self.tabController?.tabBar.isTranslucent = false
        self.tabController?.tabBar.tintColor = UIColor.black
        self.tabController?.presenter = self.presenter
        self.tabController!.delegate = self
        // setup child coordinators
        // setup tab view controllers
        self.viewControllers = [
        ]
        self.tabController!.viewControllers = self.viewControllers
        self.presenter.pushViewController(self.tabController!, animated: false)
        
        // Instantiate notificationVc and fetch notification count
//        NotificationCountService.instance.notificationNavCon = self.notificationCoordinator?.rootViewController
    }
    
    func refreshScreensForNotification(_ pushNotification: PushNotification? = nil) {
        Logger.log()
        // refresh notification list
//        if let notificationController = self.notificationCoordinator?.rootViewController, let index = self.tabController?.viewControllers?.index(of: notificationController), self.tabController?.selectedIndex == index {
//            self.notificationCoordinator?.refreshData(true)
//        } else {
//            self.notificationCoordinator?.refreshData()
//        }
        // refresh active screen(s)
        if let data = pushNotification {
            Logger.log("category \(data.category), notification id: \(data.notification_id)")
            switch data.category {
//            case CANotificationCategory.ADJUSTMENT, CANotificationCategory.POINT, CANotificationCategory.POINT_EXPIRED, CANotificationCategory.REDEMPTION:
//                self.rewardCoordinator?.refreshAccountInfoData()
//                self.refreshPointsHistoryScreens()
//
//            case CANotificationCategory.CAMPAIGN_MY_REWARD, CANotificationCategory.REWARD:
//                self.rewardCoordinator?.refreshMyRewardsData()
//
//            case CANotificationCategory.PURCHASE:
//                if let receiptNo = data.item_id {
//                    self.refreshPointsHistoryScreens()
//                    self.refreshReceiptDetailsScreens(receiptNo)
//                }
//
//            case CANotificationCategory.QUEST:
//                self.questCoordinator?.refreshData()
                
            default:
                break
            }
        }
    }
    
    func routePushNotification(_ pushNotification: PushNotification) {
        Logger.log("category \(pushNotification.category), notification id: \(pushNotification.notification_id)")
//        self.notificationCoordinator?.refreshData()
        switch pushNotification.category {
//        case CANotificationCategory.ADJUSTMENT:
//            self.showPointsHistoryScreenIfNeeded()
//
//        case CANotificationCategory.CAMPAIGN_MY_REWARD, CANotificationCategory.REWARD:
//            self.showMyRewardsScreen()
//            self.rewardCoordinator?.refreshMyRewardsData()
//
//        case CANotificationCategory.REDEMPTION:
//            self.showMyRewardsScreen()
//            self.rewardCoordinator?.refreshMyRewardsData()
//            self.refreshPointsHistoryScreens()
//
//        case CANotificationCategory.LINK, CANotificationCategory.MARKETING:
//            self.showLinkScreen(pushNotification)
//
//        case CANotificationCategory.POINT:
//            if let receiptNo = pushNotification.item_id {
//                self.refreshPointsHistoryScreens()
//                self.showFeedbackScreenIfNeeded(receiptNo)
//            }
//
//        case CANotificationCategory.PURCHASE:
//            if let receiptNo = pushNotification.item_id {
//                self.showReceiptDetailsScreenIfNeeded(receiptNo)
//            }
//
//        case CANotificationCategory.POINT_EXPIRY_7_DAYS, CANotificationCategory.POINT_EXPIRY_14_DAYS, CANotificationCategory.POINT_EXPIRY_30_DAYS:
//            self.showRewardsCatalogueScreen()
//
//        case CANotificationCategory.HIGHLIGHT:
//            break
//
//        case CANotificationCategory.QUEST:
//            self.showQuestScreen()
//            break
//
//        default:
//            self.showNotificationScreen()
//        }
            
        default:
            break
        }
    }
}

// MARK:- UITabBarControllerDelegate

extension MainCoordinator: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        UISelectionFeedbackGenerator().selectionChanged()
    }
}
    
// MARK:- MainTabBarController

class MainTabBarController: UITabBarController {
    
    var presenter: UINavigationController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Logger.log("Screen at: \(String(describing: self))")
        self.presenter?.isNavigationBarHidden = true
    }
    
}

extension UITabBarController {
    func orderedTabBarItemViews() -> [UIView] {
        let interactionViews = tabBar.subviews.filter({$0.isUserInteractionEnabled})
        return interactionViews.sorted(by: {$0.frame.minX < $1.frame.minX})
    }
}
