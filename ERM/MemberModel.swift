//
//  AuthModel.swift
//  DDC
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 J Powersoft Solutions. All rights reserved.
//

import Foundation

struct Tac: Codable {
    let tacsent: Int
    let tac_prefix: String
}

struct Verification: Codable {
    let verification_step: String
    let card_type: [String]
}

struct Invitation: Codable {
    let url: String
    let background: String
}
