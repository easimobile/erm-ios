//
//  PushNotificationService.swift
//  ERM
//
//  Created by Anders Cheow on 30/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Moya
import Firebase

class NotificationTokenService {
    
    static let instance = NotificationTokenService()
    
    fileprivate var pushToken: String?
    
    fileprivate init() { }
    
    func registerOrRefreshPushToken(_ pushToken: String) {
        Logger.log("push token: \(pushToken)")
        // check if logged in
        if Defaults[.app_loggedIn] {
            // register push token
            self.registerPushToken(pushToken)
        } else {
            Logger.log("user not logged in")
            // save in memory first
            self.pushToken = pushToken
        }
    }
    
    func registerPushToken() {
        Logger.log()
        if let pushToken = self.pushToken {
            self.registerPushToken(pushToken)
        } else {
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    Logger.log("Error fetching remote instange ID: \(error)")
                } else if let instanceIDToken = result?.token {
                    self.registerPushToken(instanceIDToken)
                }
            }
        }
    }
    
    func removeAndResetPushToken() {
        Logger.log()
        if let registeredPushToken = Defaults[.app_registeredPushToken] {
            self.removePushToken(registeredPushToken)
        }
    }
    
    fileprivate func registerPushToken(_ pushToken: String) {
        Logger.log("push token: \(pushToken)")
        if !Defaults[.app_isPushTokenRegistered] {
            let model = RegisterPushTokenRequest(pushToken: pushToken)
            apiProvider.customBasicRequest(Api.registerPushToken(model: model), whenSuccess: {
                Logger.log("registerPushToken: push token registered")
                Defaults[.app_isPushTokenRegistered] = true
            }, whenError: { error in
                Logger.log("registerPushToken: \(error)")
                Defaults[.app_isPushTokenRegistered] = false
            })
        }
    }
    
    fileprivate func removePushToken(_ pushToken: String) {
        Logger.log("push token: \(pushToken)")
        let model = RemovePushTokenRequest(pushToken: pushToken)
        apiProvider.customBasicRequest(Api.removePushToken(model: model), whenSuccess: {
            Logger.log("removePushToken: push token removed")
            Defaults[.app_isPushTokenRegistered] = false
            self.resetFcmToken()
        }, whenError: { error in
            Logger.log("removePushToken: \(error)")
            self.resetFcmToken()
        })
    }
    
    fileprivate func resetFcmToken() {
        Defaults[.app_registeredPushToken] = nil
        // reset firebase id
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                Logger.log("Error fetching remote instange ID: \(error)")
            } else if result?.token == self.pushToken {
                InstanceID.instanceID().deleteID { (error) in
                    Logger.log("fcm reset")
                }
            }
        }
    }
}
