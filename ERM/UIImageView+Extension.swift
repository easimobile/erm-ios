//
//  UIImageView+Extension.swift
//  ERM
//
//  Created by Anders Cheow on 21/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}
