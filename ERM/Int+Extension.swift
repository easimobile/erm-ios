//
//  Int+Extension.swift
//  ERM
//
//  Created by Anders Cheow on 24/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

extension Int {
    
    func formatAmount() -> String {
        let double = Double(self) / 100.0
        return double.formatAmount()
    }
}
