//
//  AuthModel.swift
//  DDC
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 J Powersoft Solutions. All rights reserved.
//

import Foundation

struct Auth: Codable {
    let token: String
    let username: String
}
