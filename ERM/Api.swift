//
//  Api.swift
//  ERM
//
//  Created by Anders Cheow on 30/04/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import Moya
import SwiftyUserDefaults
import Alamofire

let AUTHORISATION = ""
let BASE_URL = Configuration.BASE_URL

let apiProvider = ApiProvider()

// MARK: - Provider setup

class DefaultAlamofireManager: Alamofire.SessionManager {
    static let sharedManager: DefaultAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 10 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 10 // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return DefaultAlamofireManager(configuration: configuration)
    }()
}

// typealias ApiProvider = MoyaProvider<Api>

class ApiProvider: MoyaProvider<Api> {
    convenience init() {
        self.init(manager: DefaultAlamofireManager.sharedManager, plugins: [NetworkLoggerPlugin(verbose: true)])
    }
    
    func customRequest<T : Codable>(_ target: Api, whenSuccess: @escaping (T) -> Void, whenError: @escaping (String) -> Void) {
        super.request(target, callbackQueue: .none, progress: .none, completion: { result in
            switch result {
            case let .success(response):
                if let data = try? JSONDecoder().decode(BaseAPIModel<T>.self, from: response.data) {
                    if data.isValid(), let result = data.data {
                        whenSuccess(result)
                    } else {
                        whenError(data.toErrorReadableString())
                    }
                } else {
                    whenError("api_error_generic".localized)
                }
            case let .failure(error):
                print(error)
                if let response = error.response {
                    if response.statusCode == 401 {
                        (UIApplication.shared.delegate as? AppDelegate)?.appCoordinator?.didLogout()
                    } else if let data = try? JSONDecoder().decode(BasicAPIModel.self, from: response.data) {
                        whenError(data.toErrorReadableString())
                    } else {
                        whenError(error.localizedDescription)
                    }
                } else {
                    whenError(error.localizedDescription)
                }
            }
        })
    }
    
    func customBasicRequest(_ target: Api, whenSuccess: @escaping () -> Void, whenError: @escaping (String) -> Void) {
        super.request(target, callbackQueue: .none, progress: .none, completion: { result in
            switch result {
            case let .success(response):
                if let data = try? JSONDecoder().decode(BasicAPIModel.self, from: response.data) {
                    if data.isValid() {
                        whenSuccess()
                    } else {
                        whenError(data.toErrorReadableString())
                    }
                } else {
                    whenError("api_error_generic".localized)
                }
            case let .failure(error):
                print(error)
                if let response = error.response {
                    if response.statusCode == 401 {
                        (UIApplication.shared.delegate as? AppDelegate)?.appCoordinator?.didLogout()
                    } else if let data = try? JSONDecoder().decode(BasicAPIModel.self, from: response.data) {
                        whenError(data.toErrorReadableString())
                    } else {
                        whenError(error.localizedDescription)
                    }
                } else {
                    whenError(error.localizedDescription)
                }
            }
        })
    }
}

// MARK: - Provider support

public enum Api {
    case checkVersion(model: CheckVersionRequest)
    case registerPushToken(model: RegisterPushTokenRequest)
    case removePushToken(model: RemovePushTokenRequest)
}

extension Api: TargetType {

    // The target's base `URL`.
    public var baseURL: URL {
        return URL(string: BASE_URL)!
    }
    
    // The path to be appended to `baseURL` to form the full `URL`.
    public var path: String {
        switch self {
        case .checkVersion:
            return "/ddcard/version/check"
            
        case .registerPushToken:
            return "/ddcard/apptoken"
            
        case .removePushToken:
            return "/ddcard/apptoken"
        }
    }
    
    // The HTTP method used in the request.
    public var method: Moya.Method {
        switch self {
        default:
            return .post
        }
    }
    
    // Provides stub data for use in testing.
    public var sampleData: Data {
        return Data()
    }
    
    // The type of HTTP task to be performed.
    public var task: Task {
        switch self {
        case .checkVersion(let model):
            return .requestParameters(parameters: model.toDict(), encoding: URLEncoding.default)
            
        case .registerPushToken(let model):
            return .requestParameters(parameters: model.toDict(), encoding: URLEncoding.default)
            
        case .removePushToken(let model):
            return .requestParameters(parameters: model.toDict(), encoding: URLEncoding.default)
        }
    }
    
    // Whether or not to perform Alamofire validation. Defaults to `false`.
    public var validate: Bool {
        return true
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
    
    // The headers to be used in the request.
    public var headers: [String: String]? {
//        return ["Authorization": AUTHORISATION]
        return nil
    }
}
