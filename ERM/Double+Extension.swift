//
//  Double+Extension.swift
//  ERM
//
//  Created by Anders Cheow on 24/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

extension Double {
    
    func formatAmount() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        return formatter.string(from: NSNumber(value: self))!
    }
}
