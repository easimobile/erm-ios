//
//  AppCoordinator.swift
//  ERM
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class AppCoordinator: NSObject, Coordinator {

    // parent view
    fileprivate let window: UIWindow
    
    // child views & coordinators
    fileprivate var rootViewController: UINavigationController
    fileprivate var mainCoordinator: MainCoordinator?
    
    var pendingPushNotification: PushNotification?
    
    init(window: UIWindow) {
        self.window = window
        // init root vc
        let nav = UINavigationController()
        nav.isNavigationBarHidden = true
        self.rootViewController = nav
    }
    
    func start() {
        // set root vc as window's root vc
        self.window.rootViewController = self.rootViewController
        self.window.makeKeyAndVisible()
        self.showSplashScene()
    }
    
    func routePushNotification(_ pushNotification: PushNotification) {
        Logger.log()
        if let mainCoordinator = self.mainCoordinator {
            mainCoordinator.routePushNotification(pushNotification)
        } else {
            self.pendingPushNotification = pushNotification
            Logger.log("main coordinator not ready, push notification handling postponed")
        }
    }
    
    func updateNotifications(_ pushNotification: PushNotification? = nil) {
        Logger.log()
        if let mainCoordinator = self.mainCoordinator {
            mainCoordinator.refreshScreensForNotification(pushNotification)
        }
    }
    
    fileprivate func showSplashScene() {
        Logger.log()
        let cr = SplashCoordinator(self.rootViewController)
        cr.delegate = self
        cr.start()
    }
    
    fileprivate func showLoginScene() {
//        Logger.log()
//        let nav = UINavigationController()
//        nav.isNavigationBarHidden = false
//
//        let cr = LoginCoordinator(nav)
//        cr.delegate = self
//        cr.start()
//
//        self.rootViewController = nav
//        self.window.rootViewController = self.rootViewController
    }
    
    func showLoginScene(with invitationCode: String) {
//        Logger.log()
//        let nav = UINavigationController()
//        nav.isNavigationBarHidden = false
//
//        let cr = LoginCoordinator(nav)
//        cr.delegate = self
//        cr.start(with: invitationCode)
//
//        self.rootViewController = nav
//        self.window.rootViewController = self.rootViewController
    }
    
    fileprivate func showMainScene() {
        Logger.log()
        let nav = UINavigationController()
        nav.isNavigationBarHidden = true

        let cr = MainCoordinator(nav)
        cr.delegate = self
        cr.start()

        self.mainCoordinator = cr
        self.rootViewController = nav
        self.window.rootViewController = self.rootViewController
    }
    
    fileprivate func doChangeLanguage(_ language: String) {
        Logger.log(language)
        if Defaults[.app_languageCode] != language {
            Defaults[.app_languageCode] = language
            
            Bundle.setLanguage(lang: language)
            self.showSplashScene()
        }
    }
    
    fileprivate func logout() {
        Logger.log()
        
        Defaults[.app_loggedIn] = false
        Defaults[.app_accessToken] = nil
        Defaults[.app_isPushTokenRegistered] = false
        Defaults[.app_notificationCount] = 0
//        self.mainCoordinator = nil
        
        let nav = UINavigationController()
        nav.isNavigationBarHidden = true
        self.rootViewController = nav
        
        self.window.rootViewController = self.rootViewController
        self.showSplashScene()
    }
}

// MARK:- SplashCoordinatorDelegate

extension AppCoordinator: SplashCoordinatorDelegate {
    func gotoLoginScene() {
        Logger.log()
        self.showLoginScene()
    }
    
    func gotoMainScene() {
        Logger.log()
        self.showMainScene()
    }
}

// MARK:- LoginCoordinatorDelegate

//extension AppCoordinator: LoginCoordinatorDelegate {
//    func changeLanguage(_ language: String) {
//        Logger.log()
//        self.doChangeLanguage(language)
//    }
//
//    func didLogin() {
//        Logger.log()
//        self.showMainScene()
//    }
//}

// MARK:- MainCoordinatorDelegate

extension AppCoordinator: MainCoordinatorDelegate {
    func didLogout() {
        Logger.log()
        self.logout()
    }
}
