//
//  BaseApiModel.swift
//  ERM
//
//  Created by Anders Cheow on 30/04/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import Moya

func parseResponse<T : Codable>(with response: Response, whenSuccess: (T) -> Void, whenError: (String) -> Void) {
    if let data = try? JSONDecoder().decode(BaseAPIModel<T>.self, from: response.data) {
        if data.isValid(), let result = data.data {
            whenSuccess(result)
        } else {
            whenError(data.toErrorReadableString())
        }
    } else {
        whenError("Something had gone wrong. Please try again.")
    }
}

func parseResponse(with response: Response, _ success: () -> Void, _ failed: (String) -> Void) {
    if let data = try? JSONDecoder().decode(BasicAPIModel.self, from: response.data) {
        if data.isValid() {
            success()
        } else {
            failed(data.toErrorReadableString())
        }
    } else {
        failed("Something had gone wrong. Please try again.")
    }
}

func parseErrorResponse(with error: MoyaError) -> String {
    if let response = error.response, let data = try? JSONDecoder().decode(BasicAPIModel.self, from: response.data) {
        return data.toErrorReadableString()
    } else {
        return "Something had gone wrong. Please try again."
    }
}

struct BaseAPIModel<T: Codable>: Codable {
    let error: Bool = false
    let code: Int = 0
    let message: String?
    let data: T?
    
    func isValid() -> Bool {
        return code == 200
    }
    
    func toErrorReadableString() -> String {
        return message?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
}

struct BasicAPIModel: Codable {
    let error: Bool = false
    let code: Int = 0
    let message: String?
    
    func isValid() -> Bool {
        return code == 200
    }
    
    func toErrorReadableString() -> String {
        return message?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
}
