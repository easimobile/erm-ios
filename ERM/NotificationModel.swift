//
//  NotificationModel.swift
//  ERM
//
//  Created by Anders Cheow on 22/08/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

struct PushNotification: Codable {
    let notification_id: Int
    let item_id: String?
    let category: String
    let additional_info: AdditionalInfo?
}

struct AdditionalInfo: Codable {
    let amount: Int?
    let points: Double?
    let point_balance: Double?
    let branch_code: String?
    let branch_name: String?
    let ios_url: String?
    let fallback_url: String?
}
