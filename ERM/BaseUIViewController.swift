//
//  BaseUIViewController.swift
//  ERM
//
//  Created by Anders Cheow on 23/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit

open class BaseUIViewController: UIViewController {
    
    var hideNavigationBar: Bool {
        return false
    }
    
    var transparentNavigationBar: Bool {
        return false
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.hideNavigationBar {
            self.navigationController?.isNavigationBarHidden = true
        } else if self.transparentNavigationBar {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
        } else {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupLocalisation()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.hideNavigationBar {
            self.navigationController?.isNavigationBarHidden = false
        } else if self.transparentNavigationBar {
            self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            self.navigationController?.navigationBar.shadowImage = nil
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = nil
        } else {
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    open func setupViews() {
    }
    
    open func setupLocalisation() {
    }
    
    func showAlert(_ message: String) {
        UIHelper.showAlert(self, message: message)
    }
    
    func showDismissableAlert(_ message: String, completion: (() -> Void)? = nil) {
        UIHelper.showDismissableAlert(self, message: message, completion: completion)
    }
}
