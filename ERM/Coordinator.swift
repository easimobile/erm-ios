//
//  Coordinator.swift
//  ERM
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

protocol Coordinator: NSObjectProtocol {
    func start()
}
