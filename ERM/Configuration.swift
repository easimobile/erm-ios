//
//  Configuration.swift
//  ERM
//
//  Created by Anders Cheow on 30/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

class Configuration {
    
    static let VERSION_CODE = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "1000000"
    static let BASE_URL = Bundle.main.infoDictionary!["API_DOMAIN"] as! String
    static let JPUSH_APP_KEY = Bundle.main.infoDictionary!["JPUSH_APP_KEY"] as! String
    static let APP_STORE_ID = Bundle.main.infoDictionary!["APP_STORE_ID"] as! String
    static let APP_CENTER = Bundle.main.infoDictionary!["APP_CENTER"] as! String
}
