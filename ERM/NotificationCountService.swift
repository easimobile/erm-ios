//
//  NotificationCountService.swift
//  ERM
//
//  Created by Anders Cheow on 22/08/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class NotificationCountService {
    
    static let instance = NotificationCountService()
    
    fileprivate var count: Int = 0 {
        didSet {
            Defaults[.app_notificationCount] = count
            self.notificationNavCon?.tabBarItem.badgeValue = count > 0 ? String(count) : nil
            UIApplication.shared.applicationIconBadgeNumber = count
        }
    }
    
    var notificationNavCon: UINavigationController? {
        didSet {
            self.fetchUnviewedNotificationsCount()
        }
    }
    
    init() {
        self.getDefaultNotificationCount()
    }
    
    func resetCount() {
        self.count = 0
    }
    
    func incrementCount() {
        self.count = self.count + 1
        
        self.checkCurrentTopWindow()
    }
    
    func decrementCount() {
        self.count = self.count - 1
    }
    
    func checkCurrentTopWindow() {
//        if let topVc = UIApplication.topViewController(), let _ = topVc as? NotificationsViewController {
//            delay(duration: 1.0) {
//                self.resetCount()
//            }
//        } else {
//            self.fetchUnviewedNotificationsCount()
//        }
    }
    
    func fetchUnviewedNotificationsCount() {
        Logger.log()
//        NotificationDataManager.instance.syncUnviewedNotificationsCount { (result) in
//            switch result {
//            case .success:
//                let count = NotificationDataManager.instance.fetchUnviewedNotificationsCount()
//                self.count = count
//            case .failure:
//                self.getDefaultNotificationCount()
//                break
//            }
//        }
    }
    
    fileprivate func getDefaultNotificationCount() {
        self.count = Defaults[.app_notificationCount]
    }
}
