//
//  CardModel.swift
//  DDC
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 J Powersoft Solutions. All rights reserved.
//

import Foundation

struct CardSummary: Codable {
    let card_type: String
    let card_no: String
    let balance: Double
    let color_code: String
    let histories: [CardHistory]
}

struct CardHistory: Codable {
    let id: Int
    let type: String
    let amount: Double
    let status: String
    let created_at: String
}
