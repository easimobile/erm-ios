//
//  CircleView.swift
//  ERM
//
//  Created by Anders Cheow on 21/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit

@IBDesignable
class CircleView: UIView {
    override func layoutSubviews() {
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
        self.clipsToBounds = true
    }
}
