//
//  SplashViewController.swift
//  ERM
//
//  Created by Anders Cheow on 03/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import UIKit

protocol SplashViewControllerDelegate {
}

class SplashViewController: UIViewController {
    
    var delegate: SplashViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
