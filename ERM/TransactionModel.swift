//
//  TransactionModel.swift
//  DDC
//
//  Created by Anders Cheow on 28/05/2019.
//  Copyright © 2019 J Powersoft Solutions. All rights reserved.
//

import Foundation

struct TransactionPaging: Codable {
    let transactions: Transactions
}

struct Transactions: Codable {
    let total: Int
    let current_page: Int
    let last_page: Int
    let data: [Transaction]
    
    func hasNext() -> Bool {
        return self.current_page < self.last_page
    }
}

struct Transaction: Codable {
    let id: Int
    let type: String
    let amount: Double
    let created_at: String
}
