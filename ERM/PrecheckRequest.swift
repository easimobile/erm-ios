//
//  PrecheckRequest.swift
//  ERM
//
//  Created by Anders Cheow on 02/05/2019.
//  Copyright © 2019 EASI. All rights reserved.
//

import Foundation

public class CheckVersionRequest : BaseRequest {
    override func toDict() -> [String : Any] {
        var dict = super.toDict()
        dict["version"] = Configuration.VERSION_CODE
        dict["app"] = "ddcard"
        dict["os"] = "ios"
        return dict
    }
}
